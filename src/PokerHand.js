export class PokerHand {

	constructor(hand) {
		this.hand = hand;
  }

	// verifyRule(rule, hand1, hand2) {
	// 	if(rule(hand1) && rule(hand2)){
	// 		return UsefulFunctions.highestIndex(hand1, hand2)
	// 	}
	// 	if(rule(hand1) && !rule(hand2)) { return Result.WIN }
	// 	if(!rule(hand1) && rule(hand2)) { return Result.LOSS }
	// }

	compareWith(hand) {
		const [hand1, hand2] = [this.splitter(), hand.splitter()]
		// From ROYALFLUSH to THREEOFAKIND it does the same => Higher-order function
		// verifyRule takes a function and the two hands as parameters and returns a result

		// ROYALFLUSH
		if(this.royalflush(hand1) && this.royalflush(hand2)){ return Result.TIE }
		if(this.royalflush(hand1) && !this.royalflush(hand2)) { return Result.WIN }
		if(!this.royalflush(hand1) && this.royalflush(hand2)) { return Result.LOSS }

		// STRAIGHTFLUSH
		if((this.straightflush(hand1) && this.straightflush(hand2))){
			return UsefulFunctions.highestIndex(hand1, hand2)
		}
		if(this.straightflush(hand1) && !this.straightflush(hand2)) { return Result.WIN }
		if(!this.straightflush(hand1) && this.straightflush(hand2)) { return Result.LOSS }

		// FOUROFAKIND
		if((this.fourofakind(hand1) && this.fourofakind(hand2))){
			return UsefulFunctions.highestIndex(hand1, hand2)
		}
		if(this.fourofakind(hand1) && !this.fourofakind(hand2)) { return Result.WIN }
		if(!this.fourofakind(hand1) && this.fourofakind(hand2)) { return Result.LOSS }

		//FULLHOUSE
		if((this.fullhouse(hand1) && this.fullhouse(hand2))){
			return UsefulFunctions.highestIndex(hand1, hand2)
		}
		if(this.fullhouse(hand1) && !this.fullhouse(hand2)) { return Result.WIN }
		if(!this.fullhouse(hand1) && this.fullhouse(hand2)) { return Result.LOSS }

		//FLUSH
		if((this.flush(hand1) && this.flush(hand2))){
			return UsefulFunctions.highestIndex(hand1, hand2)
		}
		if(this.flush(hand1) && !this.flush(hand2)) { return Result.WIN }
		if(!this.flush(hand1) && this.flush(hand2)) { return Result.LOSS }

		//STRAIGHT
		if((this.straight(hand1) && this.straight(hand2))){
			return UsefulFunctions.highestIndex(hand1, hand2)
		}
		if(this.straight(hand1) && !this.straight(hand2)) { return Result.WIN }
		if(!this.straight(hand1) && this.straight(hand2)) { return Result.LOSS }

		//THREEOFAKIND
		if((this.threeofakind(hand1) && this.threeofakind(hand2))){
			return UsefulFunctions.highestIndex(hand1, hand2)
		}
		if(this.threeofakind(hand1) && !this.threeofakind(hand2)) { return Result.WIN }
		if(!this.threeofakind(hand1) && this.threeofakind(hand2)) { return Result.LOSS }

		//TWOPAIRS
		const [index1, index2, others1, others2] = UsefulFunctions.organizePairs(hand1, hand2)

		if((this.twopairs(hand1) && this.twopairs(hand2))){
			if((index1[0]) > (index2[0])){ return Result.WIN }

			if((index1[0]) < (index2[0])){ return Result.LOSS }

			if(index1[0] == index2[0]){
				if(index1[1] > index2[1]){ return Result.WIN }
				if(index1[1] < index2[1]){ return Result.LOSS }
			}

			if(others1[0] > others2[0]){ return Result.WIN }

			if(others1[0] < others2[0]){ return Result.LOSS }

			if(others1[0] == others2[0]){ return Result.TIE }
		}
		if(this.twopairs(hand1) && !this.twopairs(hand2)) { return Result.WIN }
		if(!this.twopairs(hand1) && this.twopairs(hand2)) { return Result.LOSS }

		//PAIR
		if((this.pair(hand1) && this.pair(hand2))){
			if((index1[0]) > (index2[0])){ return Result.WIN }

			if((index1[0]) < (index2[0])){ return Result.LOSS }

			for(let i = 0; i < 5; i++){
				if(UsefulFunctions.getIndex(hand1)[i] > UsefulFunctions.getIndex(hand2)[i]){ return Result.WIN }
				if(UsefulFunctions.getIndex(hand1)[i] < UsefulFunctions.getIndex(hand2)[i]){ return Result.LOSS }
			}

			return Result.TIE

		}
		if(this.pair(hand1) && !this.pair(hand2)) { return Result.WIN }
		if(!this.pair(hand1) && this.pair(hand2)) { return Result.LOSS }

		//HIGHCARD
		for(let i = 0; i < 5; i++){
			if(UsefulFunctions.getIndex(hand1)[i] > UsefulFunctions.getIndex(hand2)[i]){ return Result.WIN }
			if(UsefulFunctions.getIndex(hand1)[i] < UsefulFunctions.getIndex(hand2)[i]){ return Result.LOSS }
		}
		return Result.TIE
	}

	splitter(){
		const splittedHand = this.hand.split(' ')
		let values = []
		let colours = []
		for(let i = 0; i < splittedHand.length; i++){
			if(splittedHand[i][0] == '1'){
				values.push('10');
				colours.push(splittedHand[i][2]);
			} else {
				values.push(splittedHand[i][0]);
				colours.push(splittedHand[i][1]);
			}
		}
		return [values, colours]
	}

	royalflush(hand){
		return (UsefulFunctions.sameSuit(hand) && UsefulFunctions.isRoyal(hand)) ? true : false
	}

	straightflush(hand){
		return (UsefulFunctions.sameSuit(hand) && UsefulFunctions.consecutive(hand)) ? true : false
	}

	fourofakind(hand){
		return Object.values(UsefulFunctions.countCards(hand)).includes(4) ? true : false
	}

	fullhouse(hand){
		return (Object.values(UsefulFunctions.countCards(hand)).includes(2) && Object.values(UsefulFunctions.countCards(hand)).includes(3)) ? true : false
	}

	flush(hand){
		return (UsefulFunctions.consecutive(hand) == false && UsefulFunctions.sameSuit(hand))? true : false
	}

	straight(hand){
		return ((Object.keys(UsefulFunctions.countCards(hand)).length ==5) && UsefulFunctions.consecutive(hand) && UsefulFunctions.sameSuit(hand) == false)? true : false
	}

	threeofakind(hand){
		return (Object.values(UsefulFunctions.countCards(hand)).includes(1) && Object.values(UsefulFunctions.countCards(hand)).includes(3)) ? true : false
	}

	twopairs(hand){
		return (UsefulFunctions.checkPairs(hand)[0] == 2 && Object.values(UsefulFunctions.checkPairs(hand)[1]).includes(1) && Object.values(UsefulFunctions.checkPairs(hand)[1]).includes(2) && Object.values(UsefulFunctions.checkPairs(hand)[1]).includes(3) == false) ? true : false
	}

	pair(hand){
		return (UsefulFunctions.checkPairs(hand)[0] == 1 && Object.values(UsefulFunctions.checkPairs(hand)[1]).includes(1) && Object.values(UsefulFunctions.checkPairs(hand)[1]).includes(2) && Object.values(UsefulFunctions.checkPairs(hand)[1]).includes(3) == false) ? true : false
	}
} //end of the class

export const Cards = {
	cards: 			['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'],
	suits: 			['S', 'H', 'D', 'C']
};

export const UsefulFunctions = {
    isRoyal: function(hand) {
        return Cards.cards.slice(Math.max(Cards.cards.length-5 , 1)).every(function(val) { return hand[0].indexOf(val) >= 0; });
    },
		consecutive: function(hand){
				const lowestIndex = UsefulFunctions.getIndex(hand).pop();
				const cardRange = Cards.cards.slice(lowestIndex, lowestIndex + 5)
				// weakest straight
				if([ '2', '3', '4', '5', 'A' ].every(function(val) { return hand[0].indexOf(val) >= 0; }) && !UsefulFunctions.isRoyal(hand)){
					return true
				} else {
					return hand[0].every(function(val) { return cardRange.indexOf(val) >= 0; })
				}
		},
		getIndex: function(hand){
			let indexes = [];
			for(let i = 0; i < hand[0].length; i++){
				indexes.push(Cards.cards.indexOf(hand[0][i]))
			}
			indexes = indexes.sort(function(a, b){return b - a})
			return indexes
		},
		sameSuit : function(hand){
			return hand[1].every((val, i, arr) => val === arr[0])
		},

		highestIndex: function(hand1, hand2){
			for(let i = 0; i < UsefulFunctions.getIndex(hand1).length; i++){
				if(UsefulFunctions.getIndex(hand1)[i] == UsefulFunctions.getIndex(hand2)[i]){
					return Result.TIE
				} else if(UsefulFunctions.getIndex(hand1)[0] > UsefulFunctions.getIndex(hand2)[0]){
					return Result.WIN
				} else {
					return Result.LOSS
				}
			}
		},

		countCards: function(hand){
			let cardCounter = {};
			hand[0].forEach(function(x) { cardCounter[x] = (cardCounter[x] || 0)+1; });
			return cardCounter
		},

		checkPairs: function(hand){
			const myCounts = UsefulFunctions.countCards(hand);
			let twoPairs = [];
			let count = 0;

			for (let values in myCounts) {
				twoPairs.push([myCounts[values], values]);
				if(myCounts[values] == 2){
					count += 1
				}
			}
			return [count, myCounts]
		},

		countPairIndexes: function(hand){
			let index = [];
			let others = [];
			for(let value in UsefulFunctions.countCards(hand)){
				if(UsefulFunctions.countCards(hand)[value] == 2){
					index.push(Cards.cards.indexOf(value))
				} else {
					others.push(Cards.cards.indexOf(value))
				}
			}
			return {'highIndex': index.sort(function(a, b){return b - a}), 'otherIndexes': others.sort(function(a, b){return b - a})}
		},

		organizePairs: function(hand1, hand2){
			return [
				UsefulFunctions.countPairIndexes(hand1).highIndex, UsefulFunctions.countPairIndexes(hand2).highIndex,
				UsefulFunctions.countPairIndexes(hand1).otherIndexes, UsefulFunctions.countPairIndexes(hand2).otherIndexes
			]
		}
}

export const Result = {
	WIN: 1,
	LOSS: 2,
	TIE: 3
};

export default PokerHand;
