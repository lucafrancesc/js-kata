import PokerHand, { Result, Cards, UsefulFunctions} from './PokerHand.js';

describe('PokerHand', () => {
// USEFUL FUNCTIONS
	describe('splitter()', () => {
		it(`splits hand1 into 2 arrays`, () => {
			const hand1 = new PokerHand('AC KS 5S 8C 7H');
			expect(hand1.splitter()).toEqual([[ 'A', 'K', '5', '8', '7' ],[ 'C', 'S', 'S', 'C', 'H' ]]);
		});
		it(`splits hand2 into 2 arrays`, () => {
			const hand2 = new PokerHand('4S 5S 8C AS AD');
			expect(hand2.splitter()).toEqual([[ '4', '5', '8', 'A', 'A' ],[ 'S', 'S', 'C', 'S', 'D' ]]);
		});
	});

	describe('isRoyal()', () => {
		it(`checks if the cards are the highest in the stack - true`, () => {
			const hand1 = new PokerHand('KC QC JC AC 10C');
			expect(UsefulFunctions.isRoyal(hand1.splitter())).toBe(true);
		});

		it(`checks if the cards are the highest in the stack - false`, () => {
			const hand2 = new PokerHand('4S 5S 8C AS AD');
			expect(UsefulFunctions.isRoyal(hand2.splitter())).toBe(false);
		});
	});

	describe('consecutive()', () => {
		it(`checks if the cards are the highest in the stack - true`, () => {
			const hand1 = new PokerHand('KC QC JC 9C 10C');
			expect(UsefulFunctions.consecutive(hand1.splitter())).toBe(true);
		});

		it(`checks if the cards are the highest in the stack - false`, () => {
			const hand2 = new PokerHand('4S 5S 8C AS AD');
			expect(UsefulFunctions.consecutive(hand2.splitter())).toBe(false);
		});
	});

	describe('sameSuit()', () => {
		it(`checks if the cards share the same suit - true`, () => {
			const hand1 = new PokerHand('KC QC JC AC 10C');
			expect(UsefulFunctions.sameSuit(hand1.splitter())).toBe(true);
		});

		it(`checks if the cards share the same suit - false`, () => {
			const hand2 = new PokerHand('4S 5S 8C AS AD');
			expect(UsefulFunctions.sameSuit(hand2.splitter())).toBe(false);
		});
	});

	describe('getIndex()', () => {
		it(`returns a list of indexes`, () => {
			const hand1 = new PokerHand('KC QC JC 9C 10C');
			expect(UsefulFunctions.getIndex(hand1.splitter())).toEqual([11, 10, 9, 8, 7]);
		});
	});

	describe('highestIndex()', () => {
		it(`checks the highest index - true`, () => {
			const hand1 = new PokerHand('KC QC JC AC 10C');
			const hand2 = new PokerHand('4S 5S 8C AS AD');
			expect(UsefulFunctions.highestIndex(hand1.splitter(), hand2.splitter())).toBe(Result.TIE);
		});
		it(`checks the highest index - true`, () => {
			const hand1 = new PokerHand('KC QC JC 9C 10C');
			const hand2 = new PokerHand('4S 5S 8C AS AD');
			expect(UsefulFunctions.highestIndex(hand1.splitter(), hand2.splitter())).toBe(Result.LOSS);
		});
		it(`checks the highest index - true`, () => {
			const hand1 = new PokerHand('KC QC JC 9C 10C');
			const hand2 = new PokerHand('4S 5S 8C 2S 4D');
			expect(UsefulFunctions.highestIndex(hand1.splitter(), hand2.splitter())).toBe(Result.WIN);
		});
	});

	describe('countCards()', () => {
		it(`returns an obj with the cards and their count`, () => {
			const hand1 = new PokerHand('3C 3H 3D 3S 4C');
			expect(UsefulFunctions.countCards(hand1.splitter())).toEqual({"3": 4, "4": 1});
		});

		it(`returns an obj with the cards and their count`, () => {
			const hand1 = new PokerHand('KC QC JC 9C 10C');
			expect(UsefulFunctions.countCards(hand1.splitter())).toEqual({"10": 1, "9": 1, "J": 1, "K": 1, "Q": 1});
		});
	});

	describe('countPairIndexes()', () => {
		it(`returns an array with the cards indexes`, () => {
			const hand1 = new PokerHand('3C 3H 4D 5S 4C');
			expect(UsefulFunctions.countPairIndexes(hand1.splitter())).toEqual( {"highIndex": [2, 1], "otherIndexes": [3]});
		});
	});
	describe('organizePairs()', () => {
		it(`returns an array with the hands indexes`, () => {
			const hand1 = new PokerHand('3C 3H 4D 5S 4C');
			const hand2 = new PokerHand('KC QC JC QS KS');
			expect(UsefulFunctions.organizePairs(hand1.splitter(), hand2.splitter())).toEqual([[2, 1], [11, 10], [3], [9]]);
		});
	});

	describe('checkPairs()', () => {
		it(`checks if a card appears once or twice in a hand`, () => {
			const hand1 = new PokerHand('3C 3H 4D 5S 4C');
			const hand2 = new PokerHand('KC QC JC QS KS');
			expect(UsefulFunctions.organizePairs(hand1.splitter(), hand2.splitter())).toEqual([[2, 1], [11, 10], [3], [9]]);
		});
	});

// FUNCTIONS COMBINATIONS
	describe('royalflush()', () => {
		it(`same suit from 10 to Ace`, () => {
			const hand1 = new PokerHand('KC QC JC AC 10C');
			expect(hand1.royalflush(hand1.splitter())).toBe(true);
		});
		it(`same suit from 10 to Ace`, () => {
			const hand2 = new PokerHand('4S 5S 8C AS AD');
			expect(hand2.royalflush(hand2.splitter())).toBe(false);
		});
	});

	describe('straightflush()', () => {
		it(`same suit increasing value - true`, () => {
			const hand1 = new PokerHand('9C 7C 5C 6C 8C');
			expect(hand1.straightflush(hand1.splitter())).toBe(true);
		});

		it(`same suit increasing value - false`, () => {
			const hand2 = new PokerHand('9H 7D 10H JH 8H');
			expect(hand2.straightflush(hand2.splitter())).toBe(false);
		});
	});

	describe('fourofakind()', () => {
		it(`four card same value - true`, () => {
			const hand1 = new PokerHand('3C 3H 3D 3S 4C');
			expect(hand1.fourofakind(hand1.splitter())).toBe(true);
		});

		it(`four card same value - false`, () => {
			const hand2 = new PokerHand('9H 3D 10H JH 8H');
			expect(hand2.fourofakind(hand2.splitter())).toBe(false);
		});
	});

	describe('fullhouse()', () => {
		it(`has three of a kind and a pair - true`, () => {
			const hand1 = new PokerHand('AC AH AD 2S 2C');
			expect(hand1.fullhouse(hand1.splitter())).toBe(true);
		});

		it(`has three of a kind and a pair - false`, () => {
			const hand2 = new PokerHand('9H 3D 10H JH 8H');
			expect(hand2.fullhouse(hand2.splitter())).toBe(false);
		});

		it(`has three of a kind and a pair - false`, () => {
			const hand1 = new PokerHand('3C 3H 3D AS 2C');
			expect(hand1.fullhouse(hand1.splitter())).toBe(false);
		});
	});

	describe('flush()', () => {
		it(`random cards, same suit - true`, () => {
			const hand1 = new PokerHand('3C 5C 6C JC QC');
			expect(hand1.flush(hand1.splitter())).toBe(true);
		});

		it(`random cards, same suit - false`, () => {
			const hand2 = new PokerHand('7C 6H 6D 4D 4H');
			expect(hand2.flush(hand2.splitter())).toBe(false);
		});
	});

	describe('straight()', () => {
		it(`random suit, consecutive cards`, () => {
			const hand1 = new PokerHand('3C 5C 6C 4D 7C');
			expect(hand1.straight(hand1.splitter())).toBe(true);
		});

		it(`random suit, consecutive cards - false`, () => {
			const hand2 = new PokerHand('7C 6H 6D 4D 4H');
			expect(hand2.straight(hand2.splitter())).toBe(false);
		});
	});

	describe('threeofakind()', () => {
		it(`three cards of same value + two random`, () => {
			const hand1 = new PokerHand('AC AH AD 2S 3C');
			expect(hand1.threeofakind(hand1.splitter())).toBe(true);
		});

		it(`three cards of same value + two random - false`, () => {
			const hand2 = new PokerHand('7C 6H 6D 4D 4H');
			expect(hand2.threeofakind(hand2.splitter())).toBe(false);
		});
	});

	describe('twopairs()', () => {
		it(`two pairs cards + one random - true`, () => {
			const hand1 = new PokerHand('AC AH KH 2S 2C');
			expect(hand1.twopairs(hand1.splitter())).toBe(true);
		});

		it(`two pairs cards + one random - false`, () => {
			const hand2 = new PokerHand('7C 6H 5D 4D 4H');
			expect(hand2.twopairs(hand2.splitter())).toBe(false);
		});
	});

	describe('pairs()', () => {
		it(`one pair + 3  random cards`, () => {
			const hand1 = new PokerHand('AC AH KH 3S 2C');
			expect(hand1.pair(hand1.splitter())).toBe(true);
		});

		it(`one pair + 3  random cards`, () => {
			const hand2 = new PokerHand('7C 6H 6D 4D 4H');
			expect(hand2.pair(hand2.splitter())).toBe(false);
		});
	});

	// COMPARE WITH
	describe('compareWith()', () => {
		describe('ROYALFLUSH', () => {
			it(`royalflush`, () => {
				const hand1 = new PokerHand('KC QC JC AC 10C');
				const hand2 = new PokerHand('4S 5S 8C AS AD');
				expect(hand1.compareWith(hand2)).toBe(Result.WIN);
			});

			it(`royalflush - opposite hands`, () => {
				const hand1 = new PokerHand('KC QC JC AC 10C');
				const hand2 = new PokerHand('4S 5S 8C AS AD');
				expect(hand2.compareWith(hand1)).toBe(Result.LOSS);
			});
		});
		describe('STRAIGHTFLUSH', () => {
			it(`same suit increasing value`, () => {
				const hand1 = new PokerHand('9C 7C 5C 6C 8C');
				const hand2 = new PokerHand('4S 5S 8C AS AD');
				expect(hand1.compareWith(hand2)).toBe(Result.WIN);
			});

			it(`same suit increasing value - opposite hands`, () => {
				const hand1 = new PokerHand('4S 5S 8C AS AD');
				const hand2 = new PokerHand('9H 7H 10H JH 8H');
				expect(hand1.compareWith(hand2)).toBe(Result.LOSS);
			});

			it(`same suit increasing value - same hands`, () => {
				const hand1 = new PokerHand('9D 7D 10D JD 8D');
				const hand2 = new PokerHand('9H 7H 10H JH 8H');
				expect(hand1.compareWith(hand2)).toBe(Result.TIE);
			});

			it(`same suit increasing value - stronger hands`, () => {
				const hand1 = new PokerHand('9D 7D 10D 6D 8D');
				const hand2 = new PokerHand('9H 7H 10H JH 8H');
				expect(hand1.compareWith(hand2)).toBe(Result.LOSS);
			});
		});

		describe('FOUROFAKIND', () => {
			it(`has four cards with the same value`, () => {
				const hand1 = new PokerHand('3C 3H 3D 3S 4C');
				const hand2 = new PokerHand('4S 5S 8C AS AD');
				expect(hand1.compareWith(hand2)).toBe(Result.WIN);
			});

			it(`has four cards with the same value - stronger hand`, () => {
				const hand1 = new PokerHand('3C 3H 3D 3S 4C');
				const hand2 = new PokerHand('5C 5H 5D 5S 4H');
				expect(hand1.compareWith(hand2)).toBe(Result.LOSS);
			});
		});

			describe('FULLHOUSE', () => {
				it(`has four cards with the same value`, () => {
					const hand1 = new PokerHand('AC AH AD 2S 2C');
					const hand2 = new PokerHand('4S 5S 8C AS KD');
					expect(hand1.compareWith(hand2)).toBe(Result.WIN);
				});

				it(`has four cards with the same value - stronger hand`, () => {
					const hand1 = new PokerHand('KC KH KD 2S 2C');
					const hand2 = new PokerHand('AC AH AD 2S 2C');
					expect(hand1.compareWith(hand2)).toBe(Result.LOSS);
				});
			});
			describe('FLUSH', () => {
				it(`random cards, same suit`, () => {
					const hand1 = new PokerHand('AC AH AD 2S 2C');
					const hand2 = new PokerHand('4S 5S 8C AS KD');
					expect(hand1.compareWith(hand2)).toBe(Result.WIN);
				});

				it(`random cards, same suit`, () => {
					const hand1 = new PokerHand('KC KH KD 2S 2C');
					const hand2 = new PokerHand('AC AH AD 2S 2C');
					expect(hand1.compareWith(hand2)).toBe(Result.LOSS);
				});
			});

			describe('FLUSH', () => {
				it(`random suit, consecutive cards`, () => {
					const hand1 = new PokerHand('9D 7C 5H 6C 8S');
					const hand2 = new PokerHand('4S 5S 8C AS KD');
					expect(hand1.compareWith(hand2)).toBe(Result.WIN);
				});

				it(`random suit, consecutive cards - same values`, () => {
					const hand1 = new PokerHand('9D 7C 5H 6C 8S');
					const hand2 = new PokerHand('9C 7D 5C 6s 8D');
					expect(hand1.compareWith(hand2)).toBe(Result.TIE);
				});

				it(`random suit, consecutive cards - stronger hand`, () => {
					const hand1 = new PokerHand('4D 7C 5H 6C 8S');
					const hand2 = new PokerHand('9D 7C 5H 6C 8S');
					expect(hand1.compareWith(hand2)).toBe(Result.LOSS);
				});
			});

			describe('THREEOFAKIND', () => {
				it(`has three of a kind and a pair`, () => {
					const hand1 = new PokerHand('AC AH AD 2S 3C');
					const hand2 = new PokerHand('4S 5S 8C AS KD');
					expect(hand1.compareWith(hand2)).toBe(Result.WIN);
				});

				it(`both have a threeofakind - stronger hand`, () => {
					const hand1 = new PokerHand('3C 3H 3D 5S 8C');
					const hand2 = new PokerHand('6C 6H 6D 4D 9H');
					expect(hand1.compareWith(hand2)).toBe(Result.LOSS);
				});
			});

			describe('TWOPAIRS', () => {
				it(`has two pairs and a card`, () => {
					const hand1 = new PokerHand('AC AH KH 2S 2C');
					const hand2 = new PokerHand('4S 5S 8C AS KD');
					expect(hand1.compareWith(hand2)).toBe(Result.WIN);
				});

				it(`both have two pair - same first index, different second`, () => {
					const hand1 = new PokerHand('AC AH 8H 2S 2C');
					const hand2 = new PokerHand('4S 8S 8C AS AD');
					expect(hand1.compareWith(hand2)).toBe(Result.LOSS);
				});

				it(`both have two pair - same pair indexes, different spare`, () => {
					const hand1 = new PokerHand('AC AH 5H 5S 2C');
					const hand2 = new PokerHand('AD AS 5D 5C 8S');
					expect(hand1.compareWith(hand2)).toBe(Result.LOSS);
				});

				it(`both have two pair - same cards`, () => {
					const hand1 = new PokerHand('AC AH KH 2S 2C');
					const hand2 = new PokerHand('AD AS KD 2D 2S');
					expect(hand1.compareWith(hand2)).toBe(Result.TIE);
				});
			});

			describe('PAIR', () => {
				it(`1 pair 3 different cards - different first index`, () => {
					const hand1 = new PokerHand('AC AH KH 2S 3C');
					const hand2 = new PokerHand('KS 5S 8C QS KD');
					expect(hand1.compareWith(hand2)).toBe(Result.WIN);
				});

				it(`1 pair 3 different cards - different first index - opposite`, () => {
					const hand1 = new PokerHand('AC AH KH 2S 3C');
					const hand2 = new PokerHand('KS 5S 8C QS KD');
					expect(hand2.compareWith(hand1)).toBe(Result.LOSS);
				});

				it(`1 pair 3 different cards - same first index, different second`, () => {
					const hand1 = new PokerHand('AC KD KH 2S 3C');
					const hand2 = new PokerHand('4S 5S 8C AS AD');
					expect(hand1.compareWith(hand2)).toBe(Result.LOSS);
				});

				it(`1 pair 3 different cards - same first index, same second, different third`, () => {
					const hand1 = new PokerHand('AC AH KH 2S 3C');
					const hand2 = new PokerHand('4S 5S KC AS AD');
					expect(hand1.compareWith(hand2)).toBe(Result.LOSS);
				});

				it(`1 pair 3 different cards - same first index, same second, same third, different fourth`, () => {
					const hand1 = new PokerHand('AC AH KH 2S 5C');
					const hand2 = new PokerHand('3D 5S KC AS AD');
					expect(hand1.compareWith(hand2)).toBe(Result.LOSS);

				});

				it(`1 pair 3 different cards - all same `, () => {
					const hand1 = new PokerHand('AC AH KH 2S 5C');
					const hand2 = new PokerHand('2D 5S KC AS AD');
					expect(hand1.compareWith(hand2)).toBe(Result.TIE);
				});
		});

			describe('HIGHCARD', () => {
				it(`5 different cards - different first index`, () => {
					const hand1 = new PokerHand('AC 9H KH 2S 3C');
					const hand2 = new PokerHand('4S 5S 8C 9S QD');
					expect(hand1.compareWith(hand2)).toBe(Result.WIN);
				});

				it(`5 different cards - different first index - opposite`, () => {
					const hand1 = new PokerHand('AC 9H KH 2S 3C');
					const hand2 = new PokerHand('4S 5S 8C 9S QD');
					expect(hand2.compareWith(hand1)).toBe(Result.LOSS);
				});

				it(`5 random cards - same first index, different second`, () => {
					const hand1 = new PokerHand('AC 9H KH 2S 3C');
					const hand2 = new PokerHand('AD 9D QD 2D 3S');
					expect(hand1.compareWith(hand2)).toBe(Result.WIN);
				});

				it(`5 random cards - same first index, same second, different third`, () => {
					const hand1 = new PokerHand('AC 9H KH 2S 3C');
					const hand2 = new PokerHand('AD 8D KD 2D 3S');
					expect(hand1.compareWith(hand2)).toBe(Result.WIN);
				});

				it(`5 random cards - same first index, same second, same third, different fourth`, () => {
					const hand1 = new PokerHand('AC 9H KH 2S 4C');
					const hand2 = new PokerHand('AD 9D KD 2D 3S');
					expect(hand1.compareWith(hand2)).toBe(Result.WIN);
				});

				it(`5 random cards - same first index, same second, same third, same fourth, different fifth`, () => {
					const hand1 = new PokerHand('AC 9H KH 3S 4C');
					const hand2 = new PokerHand('AD 9D KD 2D 4S');
					expect(hand1.compareWith(hand2)).toBe(Result.WIN);
				});

				it(`5 random cards - all same `, () => {
					const hand1 = new PokerHand('AC 9H KH 2S 3C');
					const hand2 = new PokerHand('AD 9D KD 2D 3S');
					expect(hand1.compareWith(hand2)).toBe(Result.TIE);
				});
		});
	}); //end of compare

	describe(`SHOWDOWN`, () => {
		// ROYALFLUSH vs ROYALFLUSH
		it(`ROYALFLUSH vs ROYALFLUSH`, () => {
			const hand1 = new PokerHand('KC QC JC AC 10C');
			const hand2 = new PokerHand('KH QH JH AH 10H');
			expect(hand1.compareWith(hand2)).toBe(Result.TIE);
		});
		// ROYALFLUSH END

		// ROYALFLUSH vs STRAIGHTFLUSH
		it(`ROYALFLUSH vs STRAIGHTFLUSH`, () => {
			const hand1 = new PokerHand('KC QC JC AC 10C');
			const hand2 = new PokerHand('9H 7H 10H JH 8H');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		it(`ROYALFLUSH vs STRAIGHTFLUSH`, () => {
			const hand1 = new PokerHand('KC QC JC AC 10C');
			const hand2 = new PokerHand('AH 2H 3H 4H 5H');
			// console.log(hand1.verifyRule(hand1.royalflush, hand1.splitter(), hand2.splitter()));
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		it(`STRAIGHTFLUSH vs STRAIGHTFLUSH`, () => {
			const hand1 = new PokerHand('9S 7S 10S JS 8S');
			const hand2 = new PokerHand('9H 7H 10H JH 8H');
			expect(hand1.compareWith(hand2)).toBe(Result.TIE);
		});
		// STRAIGHTFLUSH END

		// ROYALFLUSH vs FOUROFAKIND
		it(`ROYALFLUSH vs FOUROFAKIND`, () => {
			const hand1 = new PokerHand('KC QC JC AC 10C');
			const hand2 = new PokerHand('9H 7H 10H JH 8H');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// STRAIGHTFLUSH vs FOUROFAKIND
		it(`STRAIGHTFLUSH vs FOUROFAKIND`, () => {
			const hand1 = new PokerHand('3C 3H 3D 3S 4C');
			const hand2 = new PokerHand('9H 7H 10H JH 8H');
			expect(hand2.compareWith(hand1)).toBe(Result.WIN);
		});
		// FOUROFAKIND END

		// ROYALFLUSH vs FULLHOUSE
		it(`ROYALFLUSH vs FULLHOUSE`, () => {
			const hand1 = new PokerHand('KC QC JC AC 10C');
			const hand2 = new PokerHand('6C 6H 6D 4D 4H');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// STRAIGHTFLUSH vs FULLHOUSE
		it(`STRAIGHTFLUSH vs FULLHOUSE`, () => {
			const hand1 = new PokerHand('9H 7H 10H JH 8H');
			const hand2 = new PokerHand('6C 6H 6D 4D 4H');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// FOUROFAKIND vs FULLHOUSE
		it(`FOUROFAKIND vs FULLHOUSE`, () => {
			const hand1 = new PokerHand('3C 3H 3D 3S 4C');
			const hand2 = new PokerHand('6C 6H 6D 4D 4H');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});
		// FULLHOUSE END

		// ROYALFLUSH vs FLUSH
		it(`ROYALFLUSH vs FLUSH`, () => {
			const hand1 = new PokerHand('KC QC JC AC 10C');
			const hand2 = new PokerHand('3C 5C 6C JC QC');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// STRAIGHTFLUSH vs FLUSH
		it(`STRAIGHTFLUSH vs FLUSH`, () => {
			const hand1 = new PokerHand('9H 7H 10H JH 8H');
			const hand2 = new PokerHand('3C 5C 6C JC QC');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// FOUROFAKIND vs FLUSH
		it(`FOUROFAKIND vs FLUSH`, () => {
			const hand1 = new PokerHand('3C 3H 3D 3S 4C');
			const hand2 = new PokerHand('3C 5C 6C JC QC');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// FULLHOUSE vs FLUSH
		it(`FULLHOUSE vs FLUSH`, () => {
			const hand1 = new PokerHand('6C 6H 6D 4D 4H');
			const hand2 = new PokerHand('3C 5C 6C JC QC');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});
		// FLUSH END


		// ROYALFLUSH vs STRAIGHT
		it(`ROYALFLUSH vs STRAIGHT`, () => {
			const hand1 = new PokerHand('KC QC JC AC 10C');
			const hand2 = new PokerHand('9D 7C 5H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// STRAIGHTFLUSH vs STRAIGHT
		it(`STRAIGHTFLUSH vs STRAIGHT`, () => {
			const hand1 = new PokerHand('9H 7H 10H JH 8H');
			const hand2 = new PokerHand('9D 7C 5H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// FOUROFAKIND vs STRAIGHT
		it(`FOUROFAKIND vs STRAIGHT`, () => {
			const hand1 = new PokerHand('3C 3H 3D 3S 4C');
			const hand2 = new PokerHand('9D 7C 5H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// FULLHOUSE vs STRAIGHT
		it(`FOUROFAKIND vs STRAIGHT`, () => {
			const hand1 = new PokerHand('6C 6H 6D 4D 4H');
			const hand2 = new PokerHand('9D 7C 5H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// FLUSH vs STRAIGHT
		it(`FLUSH vs STRAIGHT`, () => {
			const hand1 = new PokerHand('3C 5C 6C JC QC');
			const hand2 = new PokerHand('9D 7C 5H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});
		// STRAIGHT END


		// ROYALFLUSH vs THREEOFAKIND
		it(`ROYALFLUSH vs THREEOFAKIND`, () => {
			const hand1 = new PokerHand('KC QC JC AC 10C');
			const hand2 = new PokerHand('9D 9C 9H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// STRAIGHTFLUSH vs THREEOFAKIND
		it(`STRAIGHTFLUSH vs THREEOFAKIND`, () => {
			const hand1 = new PokerHand('9H 7H 10H JH 8H');
			const hand2 = new PokerHand('9D 9C 9H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// FOUROFAKIND vs THREEOFAKIND
		it(`FOUROFAKIND vs THREEOFAKIND`, () => {
			const hand1 = new PokerHand('3C 3H 3D 3S 4C');
			const hand2 = new PokerHand('9D 9C 9H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// FULLHOUSE vs THREEOFAKIND
		it(`FULLHOUSE vs THREEOFAKIND`, () => {
			const hand1 = new PokerHand('6C 6H 6D 4D 4H');
			const hand2 = new PokerHand('9D 9C 9H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// FLUSH vs THREEOFAKIND
		it(`FLUSH vs THREEOFAKIND`, () => {
			const hand1 = new PokerHand('3C 5C 6C JC QC');
			const hand2 = new PokerHand('9D 9C 9H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// STRAIGHT vs THREEOFAKIND
		it(`STRAIGHT vs THREEOFAKIND`, () => {
			const hand1 = new PokerHand('3S 5C 6C 4S 2D');
			const hand2 = new PokerHand('9D 9C 9H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});
		// THREEOFAKIND END


		// ROYALFLUSH vs TWOPAIRS
		it(`ROYALFLUSH vs TWOPAIRS`, () => {
			const hand1 = new PokerHand('KC QC JC AC 10C');
			const hand2 = new PokerHand('9D 9C 8H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// STRAIGHTFLUSH vs TWOPAIRS
		it(`STRAIGHTFLUSH vs TWOPAIRS`, () => {
			const hand1 = new PokerHand('9H 7H 10H JH 8H');
			const hand2 = new PokerHand('9D 9C 8H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// FOUROFAKIND vs TWOPAIRS
		it(`FOUROFAKIND vs TWOPAIRS`, () => {
			const hand1 = new PokerHand('3C 3H 3D 3S 4C');
			const hand2 = new PokerHand('9D 9C 8H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// FULLHOUSE vs TWOPAIRS
		it(`FOUROFAKIND vs TWOPAIRS`, () => {
			const hand1 = new PokerHand('6C 6H 6D 4D 4H');
			const hand2 = new PokerHand('9D 9C 8H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// FLUSH vs TWOPAIRS
		it(`FLUSH vs TWOPAIRS`, () => {
			const hand1 = new PokerHand('3C 5C 6C JC QC');
			const hand2 = new PokerHand('9D 9C 8H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// STRAIGHT vs TWOPAIRS
		it(`STRAIGHT vs TWOPAIRS`, () => {
			const hand1 = new PokerHand('3S 5C 6C 4S 2D');
			const hand2 = new PokerHand('9D 9C 8H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// THREEOFAKIND vs TWOPAIRS
		it(`THREEOFAKIND vs TWOPAIRS`, () => {
			const hand1 = new PokerHand('3S 3C 6C 4S 3D');
			const hand2 = new PokerHand('9D 9C 8H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});
		// TWOPAIRS END


		// ROYALFLUSH vs PAIR
		it(`ROYALFLUSH vs PAIR`, () => {
			const hand1 = new PokerHand('KC QC JC AC 10C');
			const hand2 = new PokerHand('9D 9C 7H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// STRAIGHTFLUSH vs PAIR
		it(`STRAIGHTFLUSH vs PAIR`, () => {
			const hand1 = new PokerHand('9H 7H 10H JH 8H');
			const hand2 = new PokerHand('9D 9C 7H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// FOUROFAKIND vs PAIR
		it(`FOUROFAKIND vs PAIR`, () => {
			const hand1 = new PokerHand('3C 3H 3D 3S 4C');
			const hand2 = new PokerHand('9D 9C 7H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// FULLHOUSE vs PAIR
		it(`FULLHOUSE vs PAIR`, () => {
			const hand1 = new PokerHand('6C 6H 6D 4D 4H');
			const hand2 = new PokerHand('9D 9C 7H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// FLUSH vs PAIR
		it(`FLUSH vs PAIR`, () => {
			const hand1 = new PokerHand('3C 5C 6C JC QC');
			const hand2 = new PokerHand('9D 9C 7H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// STRAIGHT vs PAIR
		it(`STRAIGHT vs PAIR`, () => {
			const hand1 = new PokerHand('3S 5C 6C 4S 2D');
			const hand2 = new PokerHand('9D 9C 7H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// THREEOFAKIND vs PAIR
		it(`THREEOFAKIND vs PAIR`, () => {
			const hand1 = new PokerHand('3S 3C 6C 4S 3D');
			const hand2 = new PokerHand('9D 9C 7H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// TWOPAIRS vs PAIR
		it(`TWOPAIRS vs PAIR`, () => {
			const hand1 = new PokerHand('9S 9D 7D 7C 5S');
			const hand2 = new PokerHand('9D 9C 7H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});
		// PAIR END


		// ROYALFLUSH vs HIGHCARD
		it(`ROYALFLUSH vs HIGHCARD`, () => {
			const hand1 = new PokerHand('KC QC JC AC 10C');
			const hand2 = new PokerHand('AD 9C 7H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// STRAIGHTFLUSH vs HIGHCARD
		it(`STRAIGHTFLUSH vs HIGHCARD`, () => {
			const hand1 = new PokerHand('9H 7H 10H JH 8H');
			const hand2 = new PokerHand('AD 9C 7H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// FOUROFAKIND vs HIGHCARD
		it(`FOUROFAKIND vs HIGHCARD`, () => {
			const hand1 = new PokerHand('3C 3H 3D 3S 4C');
			const hand2 = new PokerHand('AD 9C 7H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// FULLHOUSE vs HIGHCARD
		it(`FULLHOUSE vs HIGHCARD`, () => {
			const hand1 = new PokerHand('6C 6H 6D 4D 4H');
			const hand2 = new PokerHand('AD 9C 7H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// FLUSH vs HIGHCARD
		it(`FLUSH vs HIGHCARD`, () => {
			const hand1 = new PokerHand('3C 5C 6C JC QC');
			const hand2 = new PokerHand('AD 9C 7H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// STRAIGHT vs HIGHCARD
		it(`STRAIGHT vs HIGHCARD`, () => {
			const hand1 = new PokerHand('3S 5C 6C 4S 2D');
			const hand2 = new PokerHand('AD 9C 7H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// THREEOFAKIND vs HIGHCARD
		it(`THREEOFAKIND vs HIGHCARD`, () => {
			const hand1 = new PokerHand('3S 3C 6C 4S 3D');
			const hand2 = new PokerHand('AD 9C 7H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// TWOPAIRS vs HIGHCARD
		it(`TWOPAIRS vs HIGHCARD`, () => {
			const hand1 = new PokerHand('9S 9D 7D 7C 5S');
			const hand2 = new PokerHand('AD 9C 7H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// PAIR vs HIGHCARD
		it(`PAIR vs HIGHCARD`, () => {
			const hand1 = new PokerHand('9S 9D 8D 7C 5S');
			const hand2 = new PokerHand('AD 9C 7H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.WIN);
		});

		// HIGHCARD vs HIGHCARD
		it(`HIGHCARD vs HIGHCARD`, () => {
			const hand1 = new PokerHand('9S KD 8D 7C 5S');
			const hand2 = new PokerHand('AD 9C 7H 6C 8S');
			expect(hand1.compareWith(hand2)).toBe(Result.LOSS);
		});
		// HIGHCARD END
	}); // SHOWDOWN END

}); //end of testing
